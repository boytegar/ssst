package id.pentacode.ssst

import android.content.Context
import android.content.SharedPreferences

class SharedData {
    companion object {
        private val MyPREFERENCES = "Preference"
        private var sharedPreferences: SharedPreferences? = null

        fun hasKey(c: Context, key: String): Boolean {
            sharedPreferences = c.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
            return sharedPreferences!!.contains(key)
        }

        fun getKey(c: Context, key: String): String? {
            sharedPreferences = c.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
            return sharedPreferences!!.getString(key, "")
        }

        fun setKey(c: Context, key: String, value: String) {
            sharedPreferences = c.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
            val editor = sharedPreferences!!.edit()
            if(value == "null")
                editor.putString(key, "")
            else
                editor.putString(key, value)
            editor.apply()
        }
    }
}