package id.pentacode.ssst

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.google.zxing.BarcodeFormat
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.DefaultDecoderFactory
import id.pentacode.ssst.SharedData
import kotlinx.android.synthetic.main.activity_scan.*
import org.json.JSONObject
import java.util.*

@SuppressLint("SetTextI18n")
class ScanActivity : AppCompatActivity() {

    lateinit var dialogLoading: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        title = "Scan QR Code"
        val formats = Arrays.asList(BarcodeFormat.QR_CODE)
        barcode_view.barcodeView.decoderFactory = DefaultDecoderFactory(formats)
        barcode_view.initializeFromIntent(intent)
        barcode_view.decodeContinuous(callback)
        barcode_view.setStatusText("Pastikan Posisi Barcode Pas")

        dialogLoading = AlertDialog.Builder(this)
            .setView(R.layout.dialog_uploading)
            .create()
        dialogLoading.setCanceledOnTouchOutside(false)
    }

    override fun onResume() {
        super.onResume()
        barcode_view.resume()
    }

    override fun onPause() {
        super.onPause()
        barcode_view.pause()
    }


    private val callback = object : BarcodeCallback {
        var lastText: String? = null
        override fun barcodeResult(result: BarcodeResult) {
            lastText = result.text
            // barcode_view.setStatusText(result.text)
            if (result.text == null || result.text == lastText) {
                barcode_view.pause()
                //postScan(lastText.toString())
                //output barcode
                submit(lastText!!)
            }
        }

        override fun possibleResultPoints(resultPoints: List<ResultPoint>) {}
    }

    fun submit(scan: String){
        dialogLoading.show()

        val url = resources.getString(R.string.base_url)
        val usherId = SharedData.getKey(this, "usherId")
        AndroidNetworking.post(url+"/checkin")
            .addBodyParameter("code", scan)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {

                override fun onResponse(response: JSONObject) {
                    dialogLoading.dismiss()
                    val status = response.getString("status")
                    val message = response.getString("message")
                    when (status) {
                        "ok" -> {
                            showDialogResult(true, message)
                        }
                        else -> {
                            showDialogResult(false, message)
                        }
                    }
                }

                override fun onError(anError: ANError?) {
                    dialogLoading.dismiss()
                    showDialogResult(false, "Tidak dapat terhubung ke sistem, mohon cek koneksi dan coba lagi")
                }
            })
    }



    fun submitBag(user_id: String){
        dialogLoading.show()

        val url = resources.getString(R.string.base_url)
        val usherId = SharedData.getKey(this, "usherId")
        AndroidNetworking.post(url+"usher/checkin-bag")
            .addBodyParameter("usher_id", usherId.toString())
            .addBodyParameter("user_id", user_id)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {

                override fun onResponse(response: JSONObject) {
                    dialogLoading.dismiss()
                    val status = response.getString("status")
                    val message = response.getString("message")
                    when (status) {
                        "ok" -> {
                            showDialogResult(true, message)
                        }
                        else -> {
                            showDialogResult(false, message)
                        }
                    }
                }

                override fun onError(anError: ANError?) {
                    dialogLoading.dismiss()
                    showDialogResult(false, "Tidak dapat terhubung ke sistem, mohon cek koneksi dan coba lagi")
                }
            })
    }

    fun showDialogResult(status: Boolean, message: String) {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.dialog_checkin_sukses, null)
        builder.setView(dialogView)

        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)
        dialogView.findViewById<View>(R.id.btn_scan).setOnClickListener {
            Handler().postDelayed({
                dialog.dismiss()
                barcode_view.resume()
            }, 200)
        }
        dialogView.findViewById<View>(R.id.btn_cancel).setOnClickListener {
            Handler().postDelayed({
                dialog.dismiss()
                finish()
            }, 200)
        }
        dialogView.findViewById<TextView>(R.id.txt_message).text = HtmlCompat.fromHtml(message, HtmlCompat.FROM_HTML_MODE_LEGACY)
        if(status){
            dialogView.findViewById<View>(R.id.header_success).visibility = View.VISIBLE
            dialogView.findViewById<View>(R.id.header_failed).visibility = View.GONE
            dialogView.findViewById<TextView>(R.id.txt_title).text = "Sukses"
        }else{
            dialogView.findViewById<View>(R.id.header_success).visibility = View.GONE
            dialogView.findViewById<View>(R.id.header_failed).visibility = View.VISIBLE
            dialogView.findViewById<TextView>(R.id.txt_title).text = "Gagal"
        }
        dialog.show()

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}
