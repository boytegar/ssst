package id.pentacode.ssst

import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import kotlinx.android.synthetic.main.activity_alat.*
import kotlinx.android.synthetic.main.dialog_checkin_sukses.view.*
import org.json.JSONObject

class AlatActivity : AppCompatActivity() {

    lateinit var dialogLoading: AlertDialog
    lateinit var txtCode: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alat)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        title = "Scan QR Code"

        dialogLoading = AlertDialog.Builder(this)
            .setView(R.layout.dialog_uploading)
            .create()
        dialogLoading.setCanceledOnTouchOutside(false)

        txtCode = findViewById(R.id.txt_code)
        txtCode.inputType = InputType.TYPE_NULL
        txtCode.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                Log.e("CODE", p0.toString())
                val tCode = p0.toString()
                if(tCode.length == 4){
                    submit(tCode)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })

        txtCode.requestFocus()
    }

    fun submit(scan: String){
        dialogLoading.show()

        val url = resources.getString(R.string.base_url)
        AndroidNetworking.post(url+"/checkin")
            .addBodyParameter("code", scan)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {

                override fun onResponse(response: JSONObject) {
                    Log.e("WEWEWEWEREA", response.toString())
                    dialogLoading.dismiss()
                    val status = response.getString("status")
                    val message = response.getString("message")
                    val data = response.getJSONObject("data")
                    val hampers_type = data.getString("hampers_type")
                    when (status) {
                        "ok" -> {
                            showDialogResult(true, message, hampers_type)
                        }
                        else -> {
                            showDialogResult(false, message, hampers_type)
                        }
                    }
                }

                override fun onError(anError: ANError?) {
                    dialogLoading.dismiss()
                    showDialogResult(
                        false,
                        "Tidak dapat terhubung ke sistem, mohon cek koneksi dan coba lagi",
                        ""
                    )
                }
            })
    }




    fun showDialogResult(status: Boolean, message: String, hampersType: String) {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.dialog_checkin_sukses, null)
        builder.setView(dialogView)

        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)
        dialogView.findViewById<View>(R.id.btn_scan).setOnClickListener {
            Handler().postDelayed({
                txtCode.setText("")
                dialog.dismiss()
            }, 200)
        }
        dialogView.findViewById<View>(R.id.btn_cancel).setOnClickListener {
            Handler().postDelayed({
                dialog.dismiss()
                finish()
            }, 200)
        }
        dialogView.findViewById<TextView>(R.id.txt_message).text = HtmlCompat.fromHtml(message, HtmlCompat.FROM_HTML_MODE_LEGACY)
        if(status){
            dialogView.findViewById<View>(R.id.header_success).visibility = View.VISIBLE
            dialogView.findViewById<View>(R.id.header_failed).visibility = View.GONE
            dialogView.findViewById<TextView>(R.id.txt_title).text = "Sukses"
            dialogView.txt_hampers.text = "Hampers : "+hampersType
        }else{
            dialogView.findViewById<View>(R.id.header_success).visibility = View.GONE
            dialogView.findViewById<View>(R.id.header_failed).visibility = View.VISIBLE
            dialogView.findViewById<TextView>(R.id.txt_title).text = "Gagal"
            dialogView.txt_hampers.text = "Hampers : "+hampersType
        }
        dialog.show()

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}
