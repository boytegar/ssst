package id.pentacode.ssst

import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_webview.*

class WebviewActivity : AppCompatActivity() {


    var navTitle: String? = ""
    var canBack: Boolean? = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        val extras = intent.extras
//        if (extras != null) {
//            targetUrl = extras.getString("targetUrl")
//            navTitle = extras.getString("navTitle")
//            canBack = extras.getBoolean("canBack")
//        }
//        if(targetUrl == null)
//            finish()

        title = navTitle

        val url = resources.getString(R.string.base_url)
        webs(url)
    }

    fun webs(url: String) {

        webview.run {
            settings.javaScriptEnabled = true
            settings.setSupportZoom(false)
            settings.allowFileAccess = true
            settings.allowContentAccess = true
            settings.allowFileAccess = true
            settings.cacheMode = WebSettings.LOAD_NO_CACHE
            loadUrl("$url/name-checkin")
        }

        when {
            Build.VERSION.SDK_INT >= 21 -> {
                webview.settings.mixedContentMode = 0
                webview.setLayerType(View.LAYER_TYPE_HARDWARE, null)
            }
            Build.VERSION.SDK_INT >= 19 ->
                webview.setLayerType(View.LAYER_TYPE_HARDWARE, null)
            else ->
                webview.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        }

        webview.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    view?.loadUrl(request!!.url.toString())
                }
                return true
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)

            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
            }
        }
    }

    override fun onBackPressed() {
        if (webview.canGoBack() && canBack!!) {
            webview.goBack()
        } else {
            webview.clearHistory()
            webview.clearCache(true)
            super.onBackPressed()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}
