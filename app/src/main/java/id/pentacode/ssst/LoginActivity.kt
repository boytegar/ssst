package id.pentacode.ssst

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.github.florent37.runtimepermission.kotlin.askPermission
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        askPermission {
            Toast.makeText(this, "Permission All Check", Toast.LENGTH_SHORT)
        }.onDeclined {
            //at least one permission have been declined by the user
        }

        // views data
        if(SharedData.hasKey(this, "name")) {
            edt_username!!.setText(SharedData.getKey(this, "usherUsername"))
//            editPassword!!.setText("002")
        }

        btn_login.setOnClickListener {
            btn_login.isClickable = false
            btn_login.text = "sedang memproses..."
            val url = resources.getString(R.string.base_url)
            val username = edt_username.text.toString()
            val password = edt_pass.text.toString()

            AndroidNetworking.post("$url/login")
                .addBodyParameter("username",username)
                .addBodyParameter("password", password)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {

                    override fun onResponse(response: JSONObject) {
                        val status = response.getString("status")
                        if(status == "ok"){
                            val data = response.getJSONObject("data")
                            val id = data.getInt("id")
                            SharedData.setKey(this@LoginActivity, "id", id.toString())
                            val name = data.getString("name")
                            SharedData.setKey(this@LoginActivity, "name", name)
                            val email = data.getString("email")
                            SharedData.setKey(this@LoginActivity, "email", email)

                            SharedData.setKey(this@LoginActivity,"isLogin","1")
                            val intent = Intent(this@LoginActivity, MainActivity::class.java)
                            startActivity(intent)
                            finish()
                        }else{
                            val message = response.getString("message")
                            Toast.makeText(this@LoginActivity,message, Toast.LENGTH_SHORT).show()
                            btn_login.isClickable = true
                            btn_login.text = "LOGIN"
                        }
                    }

                    override fun onError(anError: ANError) {
                      //  MyHelper.handleANError(anError, applicationContext, "LOGIN")
                        btn_login.isClickable = true
                        btn_login.text = "LOGIN"
                    }

                })
        }

        if(SharedData.getKey(applicationContext, "isLogin") == "1"){
            val intent = Intent(this@LoginActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
        }


    }
}
